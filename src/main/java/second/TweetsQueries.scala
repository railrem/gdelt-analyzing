package second

/**
  * Created by ALINA on 29.09.2017.
  */


import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{DoubleType, IntegerType, StringType, StructField, StructType}

object TweetsQueries {

  def main(args: Array[String]): Unit = {

    val jsonFile = "data\\sampletweets.json"

    //Initialize SparkSession
    val sparkSession = SparkSession
      .builder()
      .appName("spark-sql-basic")
      .master("local[*]")
      .getOrCreate()
    val rootLogger = Logger.getRootLogger()
    rootLogger.setLevel(Level.OFF)

    //Read json file to DF
    val tweets = sparkSession.read.json(jsonFile)

    //Show thw scheme of DF
    tweets.printSchema()

    //Read all tweets from input files (extract the message from the tweets)
    tweets.select("body").show()

    // Register the DataFrame as a SQL temporary view
    tweets.createOrReplaceTempView("tweetTable")

    //Count the most active languages
    sparkSession.sql(
      " SELECT object.twitter_lang, COUNT(*) as cnt" +
        " FROM tweetTable " +
        " GROUP BY object.twitter_lang ORDER BY cnt DESC LIMIT 25")
      .show(100)


    //Get earliest and latest tweet dates
    sparkSession.sql(
      " SELECT  MAX(postedTime),MIN(postedTime)" +
        " FROM tweetTable ")
      .show(100)

    //Get Top devices used among all Twitter users
    sparkSession.sql(
      " SELECT generator.displayName, COUNT(*) as cnt" +
        " FROM tweetTable " +
        " GROUP BY generator.displayName ORDER BY cnt DESC LIMIT 25")
      .show(100)


    //Find all the tweets by user
    sparkSession.sql(
      " SELECT body" +
        " FROM tweetTable " +
        " WHERE actor.displayName =='Jenessa Allen☯'")
      .show(100)


    //Find how many tweets each user has
    sparkSession.sql(
      " SELECT actor.displayName, COUNT(*) as cnt" +
        " FROM tweetTable " +
        " GROUP BY actor.displayName ORDER BY cnt DESC LIMIT 25")
      .show(100)

  }
}
