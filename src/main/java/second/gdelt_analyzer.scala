package second

import org.apache.spark.sql.{DataFrame, SparkSession}

object gdelt_analyzer {

  def saveDF(df: DataFrame, output: String): Unit = {

    df.repartition(1)
      .write
      .option("delimiter", "\t")
      .csv(output)
  }

  def main(args: Array[String]): Unit = {

    val gdeltFile = "data\\gdelt.csv"
    val cameoFile = "data\\CAMEO_event_codes.csv"

    //Initialize SparkSession
    val sparkSession = SparkSession
      .builder()
      .appName("spark-sql-basic")
      .master("local[*]")
      .getOrCreate()

    //Initialize GDELT DataFrame
    val gdeltDF = new GDELTdata(sparkSession, gdeltFile, cameoFile)

    //List all events which took place in recent time in USA, RUSSIA
    val gdelt = gdeltDF.readCountriesDataFrame(List("'US'", "'RS'"))
    gdelt.show(100)

    //Find the 10 most mentioned actors (persons)
    val popActor = gdeltDF.getPopularActors()
    popActor.show(100)

    //Find the 10 most mentioned events with description
    val popEvents = gdeltDF.getPopularEvents();
    val eventsWithDescription = gdeltDF.readReferenceEventDescription(popEvents).show()

  }
}
