package first

import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}
object RDDCounter {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()

    conf.setAppName("Spark Hello World")
    conf.setMaster("local[2]")
    var array=Array("s","a")
    val sc = new SparkContext(conf)
    val rootLogger = Logger.getRootLogger()
    rootLogger.setLevel(Level.OFF)
    val rddOfString = sc.textFile("numbers.txt")

    val rddaOfArrayOfInteger = rddOfString.map(a=>a.split(' '))
    val rTotal = rddaOfArrayOfInteger.map(b=>b.map(c=>c.toInt).reduce((x,y)=>x+y))
    val rMultiplesOfFive=rddaOfArrayOfInteger.map(b=>b.map(c=>c.toInt).filter((x=>x%5==0)).reduce((x,y)=>x+y))
    val rMax = rddaOfArrayOfInteger.map(b=>b.map(c=>c.toInt).max)
    val rMin =rddaOfArrayOfInteger.map(b=>b.map(c=>c.toInt).min)
    val rDistinct = rddOfString.map(a=>a.split(' ').distinct.reduce((a,b)=>a+' '+b))

    val rddOfInteger = rddOfString.flatMap(x => x.split(' '))

    val Total = rddOfInteger.map(x=>x.toInt).reduce((a,b)=>a+b)
    val MultiplesOfFive = rddOfInteger.map(x=>x.toInt).filter(x=>(x%5==0)).reduce((a, b)=>a+b)
    val Max = rddOfInteger.map(x=>x.toInt).max()
    val Min = rddOfInteger.map(x=>x.toInt).min()
    val Distinct = rddOfInteger.distinct.reduce((a,b)=>a+' '+b)


    println("Row Totals")
    rTotal.foreach(println)
    println("Row TotalsOfMult5")
    rMultiplesOfFive.foreach(println)
    println("Row Maxs")
    rMax.foreach(println)
    println("Row Mins")
    rMin.foreach(println)
    println("Row Distincts")
    rDistinct.foreach(println)
    println("File Total")
    println(Total)
    println("File TotalOfMult5")
    println(MultiplesOfFive)
    println("File Max")
    println(Max)
    println("File Min")
    println(Min)
    println("File Distincts")
    Distinct.foreach(print)
  }
}

